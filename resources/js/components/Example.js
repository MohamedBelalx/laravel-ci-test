import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
export default class Example extends Component {

    state = {
        article:[]
    };
    componentDidMount(){
        axios.get('/api/article').then((res)=>{
            this.setState({
                article: res.data.slice(0,10)
            });
            console.log(this.state.article)
        }).then((result)=>{

        });
    }
    render() {
        const {article} = this.state;
        const ArtiList = article.length ? (
          article.map(arti =>{
             return(
                    <div className="card" key={arti.id}>
                        <div className="card-header">{arti.title}</div>
                        <div className="card-body">
                            {arti.content}
                        </div>
                    </div>
             )
          })  
        ):(<h1>Loading ....</h1>);
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-8">
                        <div className="card">
                            <div className="card-header">Example Component</div>

                            <div className="card-body">{ArtiList}</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

if (document.getElementById('example')) {
    ReactDOM.render(<Example />, document.getElementById('example'));
}
